## image-editor

### How to start it?

in your console

```
$ npm install
$ webpack
$ npm start
```

in your browser go to localhost:3000/viewer.html

### Expected file

The file expected is ImageEditor.js it contains the component.
