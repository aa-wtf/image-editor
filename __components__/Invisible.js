import React from 'react';

export default class Invisible extends React.Component {
	static propTypes = {
		width: React.PropTypes.oneOfType([
			React.PropTypes.string,
			React.PropTypes.number,
		]),
		height: React.PropTypes.oneOfType([
			React.PropTypes.string,
			React.PropTypes.number,
		])
	}
	constructor(props){
		super(props);
	}
	render(){
		let style = {"width":"0px","height":"0px","position":"relative"};
		if (this.props.width){
			if (typeof(this.props.width) === "string"){
				style.width = this.props.width;
			} else {
				style.width = this.props.width + "px";
			}
		}
		if (this.props.height){
			if (typeof(this.props.height) === "string"){
				style.height = this.props.height;
			} else {
				style.height = this.props.height + "px";
			}
		}
		if (this.props.white){
			style.backgroundColor = "white";
		}
		return (<div className="Invisible" style={style}></div>);
	}
}
