import React from 'react';
import './Button.css';

export default class Button extends React.Component {
	static propTypes = {
		children:React.PropTypes.string,
		inverse:React.PropTypes.bool,
		round:React.PropTypes.bool,
		gray:React.PropTypes.bool,
		src:React.PropTypes.string,
		color:React.PropTypes.string,
		className:React.PropTypes.string,
		disabled:React.PropTypes.bool,
		corners:React.PropTypes.bool,
		type:React.PropTypes.string,
		full:React.PropTypes.bool,
		right:React.PropTypes.bool,
		style: React.PropTypes.any
	}
	constructor(props){
		super(props);
	}
	render(){
		let className = "Button color";
		if (!this.props.color){
			className += " _green";
		} else {
			className += " _" + this.props.color;
		}
		if (this.props.inverse){
			className += " _text _inverse";
		} else {
			className += " _background";
		}
		if (this.props.round){
			className += " _round";
		}
		if (this.props.gray){
			className += " _gray";
		}
		if (this.props.right){
			className += " _right";
		}
		if (this.props.full){
			className += " _full";
		}
		if (this.props.disabled){
			className += " _disabled";
		}
		let style = this.props.style || {};
		if (this.props.src){
			style['backgroundImage'] = 'url(' + this.props.src + ')';
		}
		if (this.props.corners){
			className += " _corners";
		}
		return (<button disabled={this.props.disabled} type={this.props.type}
			className={className + " " + (this.props.className || "")} onClick={this.props.onClick} style={style}>{this.props.children}</button>);
	}
}
