import './Title.css';
import React from 'react';

export default class Title extends React.Component {
	static propTypes = {
		children: React.PropTypes.any.isRequired,
		color: React.PropTypes.string
	}
	constructor(props){
		super(props);
	}
	render(){
		let className = "Title text _md color _text _" + (this.props.color || "dark-gray");
		return (<div className={className}>{this.props.children}</div>);
	}
}
