var path = require('path');
var webpack = require('webpack');

module.exports = {
	entry: './viewer.js',
	output: {
		path: path.join(__dirname, 'build'),
		filename: 'bundle.js',
		publicPath: '/'
	},
	plugins: [
		new webpack.DefinePlugin({
			IN_BROWSER: true
		})
	],
	module: {
		loaders: [
			{
				test: /\.jsx?$/,
				loader:'babel',
				exclude: /(build|node_modules)/,
				query: {
					cacheDirectory: true,
					plugins: ['transform-decorators-legacy'],
					presets: ['es2015','stage-0','react']
				}
			},
			{
				test: /(\.png|\.svg|\.gif|\.jpg)$/,
				exclude: /node_modules/,
				loader: "url",
				query:{
					limit:Number.MAX_SAFE_INTEGER
				}
			},
			{
				exclude: /node_modules/,
				test: /\.css$/,
				loader: "style!css"
			},
			{
				exclude: /node_modules/,
				test: /\.json$/,
				loader: "json"
			}
		]
	}
};
