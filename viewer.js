import React from 'react';
import ReactSlider from './__components__/ReactSlider';
import Button from './__components__/Button';
import Invisible from './__components__/Invisible';
import Title from './__components__/Title';
import ImageEditor from './ImageEditor';

import './__general__/all.css';
import './viewer.css';

function fireEvent(node,eventName){
		var doc;
		if (node.ownerDocument) {
			doc = node.ownerDocument;
		} else if (node.nodeType == 9){
			doc = node;
		} else {
			throw new Error("Invalid node passed to fireEvent: " + node.id);
		}

		if (node.dispatchEvent) {
			var eventClass = "";
			switch (eventName) {
				case "click":
				case "mousedown":
				case "mouseup":
					eventClass = "MouseEvents";
					break;

				case "focus":
				case "change":
				case "blur":
				case "select":
					eventClass = "HTMLEvents";
					break;

				default:
					throw "fireEvent: Couldn't find an event class for event '" + eventName + "'.";
					break;
			}
			var event = doc.createEvent(eventClass);
			event.initEvent(eventName, true, true);
			event.synthetic = true;
			node.dispatchEvent(event, true);
		} else  if (node.fireEvent) {
			var event = doc.createEventObject();
			event.synthetic = true; // allow detection of synthetic events
			node.fireEvent("on" + eventName, event);
		}
	}

export default class Picture extends React.Component {
	constructor(props){
		super(props);
		this.updateScale = this.updateScale.bind(this);
		this.prepareFile = this.prepareFile.bind(this);
		this.forceShowUp = this.forceShowUp.bind(this);
		this.currentFile = null;
		this.state = {
			error:null,
			buffer:null,
			scale:1,
			ratio:2,
			angle:180
		}
		this.done = this.done.bind(this);
	}
	updateScale(e){
		this.setState({'scale':e/100});
	}
	done(){
		if (this.currentFile){
			//Trying to get dataURL depending to what you did
			let dataURL = this.refs.editor.getAsDataURL((realDataURL,data)=>{
				if (err){
					return console.log(err);
				}
				let img = document.createElement('img');
				img.src = realDataURL;
				document.body.appendChild(img);
			});
			if (typeof(dataURL) !== "string" && typeof(dataURL) !== "undefined"){
				dataURL.then((realDataURL)=>{
					let img = document.createElement('img');
					img.src = realDataURL;
					document.body.appendChild(img);
				}).catch((err)=>{
					return console.log(err);
				})
			} else if (typeof(dataURL) !== "undefined"){
				let img = document.createElement('img');
				img.src = dataURL;
				document.body.appendChild(img);
			}
		}
	}
	prepareFile(e){
		if (!e.target.files[0]){
			return;
		}
		this.currentFile = e.target.files[0];
		var reader  = new FileReader();

		reader.addEventListener("load", ()=>{
			this.setState({'buffer':reader.result});
		}, false);

		reader.readAsDataURL(this.currentFile);
	}
	forceShowUp(e){
		if (!this.currentFile){
			fireEvent(this.refs.uploader,'click');
		}
	}
	render(){
		let overlayStyle = {};
		if (this.state.buffer){
			overlayStyle.display = "none";
		}

		return (<div className="profile_picture">

			<input type="file" className="_file-uploader" ref="uploader" onChange={this.prepareFile} accept="image/jpeg, image/png"/>
			<Title>TEST</Title>

			<Invisible height={100}/>

			<div className="_container">
				<ImageEditor ref="editor"
					dataURL={this.state.buffer}
					ratio={this.state.ratio}
					scale={this.state.scale}
					angle={this.state.angle}
					displayBoxWidth={300}
					onLoadError={(err)=>{console.log(err)}}/>
			</div>
			<div className="_controls-container">
				<label>Ratio</label>
				<br/>
				<input type="number" step="0.5" placeholder="ratio" value={this.state.ratio} onChange={(e)=>{this.setState({"ratio":parseFloat(e.target.value)})}}/>
				<br/>
				<label>Angle</label>
				<br/>
				<input type="number" step="90" placeholder="angle" value={this.state.angle} onChange={(e)=>{this.setState({"angle":parseInt(e.target.value) })}}/>
				<br/>
				<label>Scale</label>
				<br/>
				<ReactSlider
					style={{
						width:'100%',
						boxSizing:'border-box',
						display:'block',
						position:'relative',
						height:'50px',
						marginTop:'24px'
					}}
					defaultValue={100}
					withBars={true}
					min={100}
					max={200}
					handleStyle={[{
						'all':{
							width:'30px',
							height:'30px',
							top:'10px',
							borderRadius:'30px'
						},
						'default':{
							backgroundColor:'#009688'
						},
						'active':{
							backgroundColor:'#004D40'
						}
					}]}
					barStyle={[
						{
							height:'5px',
							top:'22.5px',
							backgroundColor:'#009688',
							borderRadius:'3px'
						},
						{
							height:'5px',
							top:'22.5px',
							backgroundColor:'#ccc',
							boxSizing: 'border-box',
							borderRadius:'3px'
						}
					]}
					onChange={this.updateScale}
				/>
				<Invisible height={24}/>
				<Button full={true} onClick={this.done} color="green">Done</Button>
			</div>

			<div className="_overlay" onClick={this.forceShowUp} style={overlayStyle}></div>
		</div>);
	}
}
import { render } from 'react-dom';
let container = document.getElementById('container');
render(<Picture/>,container);
